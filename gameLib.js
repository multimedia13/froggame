function Frog(scene, scale){
    var frog = new Sprite(scene, "frog.png", 50*scale, 40*scale);
    var currentAngle = 0;
    var maxSpeed = 10;
    var minReverseSpeed = -3
    var accelerationSpeed = 1;
    var brakeSpeed = 1;
    var rotationSpeed = 5;
    var autoRotationSpeed = 35;
    var autoMoveMinSpeed = 5;
    var autoMoveMaxSpeed = 10;

    //properties
    frog.setAngle(currentAngle);
    frog.setSpeed(0);

    //method
    frog.controlFrog = function () {
        if(this.speed == 0 && !keysDown[K_S] && !keysDown[K_DOWN]) {
            this.speed = 1;
        }

        //handle events here
        if((keysDown[K_W] || keysDown[K_UP]) && this.speed<maxSpeed) {
            this.changeSpeedBy(accelerationSpeed);
        }else if((keysDown[K_S] || keysDown[K_DOWN]) && this.speed>minReverseSpeed) {
            this.changeSpeedBy(-accelerationSpeed);
        }else if(keysDown[K_SPACE]) {
            if(this.speed>0){
                this.changeSpeedBy(-brakeSpeed);
            }else{
                this.changeSpeedBy(brakeSpeed);
            }
            if(this.speed <= brakeSpeed && this.speed >= -brakeSpeed){
                this.speed = 0;
            }
        }
        
        if(keysDown[K_A] || keysDown[K_LEFT]) {
            this.changeAngleBy(-rotationSpeed);
        }else if(keysDown[K_D] || keysDown[K_RIGHT]) {
            this.changeAngleBy(rotationSpeed);
        }
    }

    frog.autoMove = function () {
        this.changeAngleBy(randomNum(-autoRotationSpeed,autoRotationSpeed))
        this.changeSpeedBy(randomNum(autoMoveMinSpeed,autoMoveMaxSpeed))
        if(this.speed > autoMoveMaxSpeed){
            this.setSpeed(autoMoveMaxSpeed);
        }
        if(this.speed < 1){
            this.setSpeed(1)
        }
    }

    return frog;
}

function Fly(scene, scale){
    var fly = new Sprite(scene, "fly.png", 25*scale, 20*scale);
    var minSpeed = 10;
    var maxSpeed = 30;
    var rotationSpeed = 45;

    //properties
    fly.setSpeed(0);
    fly.setPosition(randomNum(0,this.cWidth),randomNum(0,this.cHeight));

    //method
    fly.autoMove = function () {
        this.changeAngleBy(randomNum(-rotationSpeed,rotationSpeed))
        this.changeSpeedBy(randomNum(minSpeed,maxSpeed))
        if(this.speed > maxSpeed){
            this.setSpeed(maxSpeed);
        }
        if(this.speed < 1){
            this.setSpeed(1)
        }
    }

    fly.respawn = function () {
        fly.setSpeed(0);
        fly.setPosition(randomNum(0,this.cWidth),randomNum(0,this.cHeight));
    }

    return fly;
}

function randomNum(min, max) {
    return Math.random() * (max - min) + min;
}